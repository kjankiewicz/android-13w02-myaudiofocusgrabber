package com.example.kjankiewicz.android_13w02_myaudiofocusgrabber

import android.content.Context
import android.media.AudioManager
import android.media.Ringtone
import android.media.RingtoneManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_my_main.*


class MyMainActivity : AppCompatActivity(), AudioManager.OnAudioFocusChangeListener {

    private lateinit var audioManager: AudioManager
    private var currentRingtone: Ringtone? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_main)

        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager

        permanentButton.setOnClickListener {
            val result = requestAudioFocus(AudioManager.AUDIOFOCUS_GAIN,
                    this, audioManager)

            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
                if (currentRingtone != null && currentRingtone!!.isPlaying)
                    currentRingtone!!.stop()
                currentRingtone = RingtoneManager.getRingtone(applicationContext, ringtoneUri)
                if (currentRingtone != null) currentRingtone!!.play()
            }
        }

        transientButton.setOnClickListener {
            val result = requestAudioFocus(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT,
                    this, audioManager)

            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                if (currentRingtone != null && currentRingtone!!.isPlaying)
                    currentRingtone!!.stop()
                currentRingtone = RingtoneManager.getRingtone(applicationContext, ringtoneUri)
                if (currentRingtone != null) currentRingtone!!.play()
            }
        }

        duckButton.setOnClickListener {
            val result = requestAudioFocus(
                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK,
                    this, audioManager)

            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                if (currentRingtone != null && currentRingtone!!.isPlaying)
                    currentRingtone!!.stop()
                currentRingtone = RingtoneManager.getRingtone(applicationContext, ringtoneUri)
                if (currentRingtone != null) currentRingtone!!.play()
            }
        }

        returnFocusButton.setOnClickListener {
            if (currentRingtone != null && currentRingtone!!.isPlaying)
                currentRingtone!!.stop()
            abandonAudioFocus(this, audioManager)
        }

    }

    public override fun onPause() {
        if (currentRingtone != null && currentRingtone!!.isPlaying)
            currentRingtone!!.stop()
        abandonAudioFocus(this, audioManager)
        super.onPause()
    }

    override fun onAudioFocusChange(focusChange: Int) {

    }
}
