package com.example.kjankiewicz.android_13w02_myaudiofocusgrabber

import android.annotation.TargetApi
import android.media.AudioAttributes
import android.media.AudioFocusRequest
import android.media.AudioManager
import android.os.Build

@TargetApi(Build.VERSION_CODES.O)
fun createAudioFocusRequest(requestType: Int,
                            audioFocusChangeListener: AudioManager.OnAudioFocusChangeListener): AudioFocusRequest {
    return AudioFocusRequest.Builder(requestType).run {
        setAudioAttributes(AudioAttributes.Builder().run {
            setUsage(AudioAttributes.USAGE_GAME)
            setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
            build()
        })
        setAcceptsDelayedFocusGain(true)
        setOnAudioFocusChangeListener(audioFocusChangeListener)
        build()
    }
}

@TargetApi(Build.VERSION_CODES.O)
fun newRequestAudioFocus(requestType: Int,
                         audioFocusChangeListener: AudioManager.OnAudioFocusChangeListener,
                         audioManager: AudioManager): Int {
    val focusRequest = createAudioFocusRequest(requestType, audioFocusChangeListener)
    return audioManager.requestAudioFocus(focusRequest)
}

@Suppress("DEPRECATION")
fun oldRequestAudioFocus(requestType: Int,
                         audioFocusChangeListener: AudioManager.OnAudioFocusChangeListener,
                         audioManager: AudioManager): Int {
    return audioManager.requestAudioFocus(audioFocusChangeListener,
            AudioManager.STREAM_MUSIC, requestType)
}

@TargetApi(Build.VERSION_CODES.O)
fun newAbandonAudioFocus(requestType: Int,
                         audioFocusChangeListener: AudioManager.OnAudioFocusChangeListener,
                         audioManager: AudioManager) {
    val focusRequest = createAudioFocusRequest(requestType, audioFocusChangeListener)
    audioManager.abandonAudioFocusRequest(focusRequest)
}

@Suppress("DEPRECATION")
fun oldAbandonAudioFocus(audioFocusChangeListener: AudioManager.OnAudioFocusChangeListener,
                         audioManager: AudioManager) {
    audioManager.abandonAudioFocus(audioFocusChangeListener)
}

fun abandonAudioFocus(audioFocusChangeListener: AudioManager.OnAudioFocusChangeListener,
                      audioManager: AudioManager) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        newAbandonAudioFocus(AudioManager.AUDIOFOCUS_GAIN, audioFocusChangeListener, audioManager)
    } else {
        oldAbandonAudioFocus(audioFocusChangeListener, audioManager)
    }
}

fun requestAudioFocus(requestType: Int,
                      audioFocusChangeListener: AudioManager.OnAudioFocusChangeListener,
                      audioManager: AudioManager): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        newRequestAudioFocus(
                requestType, audioFocusChangeListener, audioManager)
    } else {
        oldRequestAudioFocus(
                requestType, audioFocusChangeListener, audioManager)
    }
}